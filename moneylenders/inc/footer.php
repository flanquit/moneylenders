<!-- coded by alpham.co.zw -->
<div id="footer__inner">
    <div id="footer__bottom">
      <div id="footer__copyright">2018 Hammer and Tongues Holdings. All Rights Reserved.</div>
      <a id="footer__terms" href="#">Terms & conditions</a>
      <div id="footer__credits">
        <span id="footer__credits__txt">Website by</span>
        <a id="footer__credits__link" href="#" target="_blank" rel="noopener">The Residence</a>
      </div>
    </div>
  </div>

  <!-- coded by alpham.co.zw -->