<input id="nav-is-open" type="checkbox" value="" aria-hidden="true">

  <label id="nav-toggle" class="" for="nav-is-open">
    <span id="nav-toggle__list">
      <span class="nav-toggle__list__line"></span>
      <span class="nav-toggle__list__line"></span>
      <span class="nav-toggle__list__line"></span>
    </span>
    <span id="nav-toggle__cross">
      <span class="nav-toggle__cross__line"></span>
      <span class="nav-toggle__cross__line"></span>
    </span>
  </label>

  <div id="nav__content">
    <div id="nav__content__inner">
      <div id="nav__content__bg">
        <svg xmlns="http://www.w3.org/2000/svg" width="0" height="1" viewBox="0 0 1440 900"><path fill="none" stroke="#FFFFFF" stroke-width="0" stroke-miterlimit="10" d="M-41 715l398-398L-15-55"/><path fill="none" stroke="#FFFFFF" stroke-width="64" stroke-miterlimit="10" d="M541.5 122.8L701.7-37.5"/><path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-miterlimit="10" d="M1007 353L813.8 159.9 513 461.9l506.9 515.9"/><path fill="none" stroke="#FFFFFF" stroke-width="64" stroke-miterlimit="10" d="M634.8 376.7l259.4 259.4"/><path fill="none" stroke="#FFFFFF" stroke-width="64" stroke-miterlimit="10" d="M784.6 345.1L958 516.5 1499.1-37"/></svg>   
    </div>

    <!-- NAVIGATION -->
      <ul class="nav__list" id="nav__main-list">


          <li class="nav__item" data-id="home">
            <a class="navlink2" href="http://localhost/html/moneylenders/">
              <span class="nav__link__label">Home</span>
            </a>
          </li>
                  <li class="nav__item" data-id="projects">
            <a class="navlink2" href="http://localhost/html/moneylenders/loan/">
              <span class="nav__link__label">Loans</span>
            </a>
          </li>

           <li class="nav__item" data-id="processs">
            <a class="navlink" href="http://localhost/html/moneylenders/assets/applicationform/loan.pdf" target="_blank" onclick="notice()">
              <span class="nav__link__label" id="font-special-small" >Apply</span>
            </a>
            
          </li>

                  <li class="nav__item" data-id="process">
            <a class="navlink2" href="http://localhost/html/moneylenders/agents">
              <span class="nav__link__label">Agents</span>
            </a>
          </li>
          <li class="nav__item" data-id="about">
            <a class="navlink2" href="http://html.co.zw/blog/">
              <span class="nav__link__label">News/Blog</span>
            </a>
          </li>


      </ul>

      <ul class="nav__list" id="nav__side-list">
        <li class="nav__item" data-id="contact">
          <a class="navlink2" href="http://localhost/html/moneylenders/contact">
          <span class="nav__link__label">Contact</span>
          </a>
        </li>
      </ul>

      <!-- NAVIGATION END  -->
    </div>
  </div>