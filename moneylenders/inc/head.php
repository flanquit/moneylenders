    <!--START Google Analytics Tracking Code-->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-97625808-1', 'auto');
      ga('send', 'pageview');

    </script>
    <!--END Google Analytics Tracking Code-->




    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Hammer and Tongues Moneylenders | alpham.co.zw">

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="http://localhost/html/moneylanders/faviconn-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="http://localhost/html/moneylanders/faviconn-32x32.png" sizes="32x32">
    <link rel="moneylanders" href="moneylanders.json">

    <meta name="theme-color" content="#ffffff">

    <title>Hammer and Tongues Moneylenders</title>

    <script src="assets/js/lib/html-custom.min.js"></script>

    <link rel="stylesheet" href="assets/css/main.css">

    <style media="screen" type="text/css">/*<![CDATA[*/@import 'font/stylesheet.css';/*]]>*/</style>
    <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic" rel="stylesheet"> 
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

