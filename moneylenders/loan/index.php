<!-- coded by alpham.co.zw -->

<!-- This code currently has some errors on SAFARi browser so this php code below will bypass not allow safari browser to load the site by directing it to an error.php page  -->
<?php 
function get_browser_name($user_agent)
{
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';

    elseif (strpos($user_agent, 'Safari')) {
    header('location:../error');  
    }

}

// Usage:

echo get_browser_name($_SERVER['HTTP_USER_AGENT']);


?>
<!DOCTYPE html>
<html class="no-js desktop ">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <?php include '../inc/head.php'; ?>
  <script src="../assets/js/lib/html-custom.min.js"></script>
  <link rel="stylesheet" href="../assets/css/main.css"> 


  <style media="screen" type="text/css">/*<![CDATA[*/@import '../font/stylesheet.css';/*]]>*/</style>

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
 
</head>


  <body class="page-id-projects">

  
          <a id="logo">
            <img src="../assets/img/logo.png">
          </a>

<nav class="" id="nav">

    <?php include '../inc/nav2.php'; ?>  

</nav>

  <div id="page-container">

    <section class="page" id="projects-page" data-first-project-id="individual" data-title="individual Loans">
      <div id="projects-line"></div>
      <nav class="" id="projects__nav">
        <div id="projects__nav__color-overlay"></div>
        <div id="projects__nav__overlay"></div>
        <ul id="projects__nav__list">
            <li class="projects__nav__item projects__nav__item--selected" data-id="individual">
              <a class="projects__nav__link" href="individual" data-ajax="false">
                  <div class="projects__nav__index">
                    <div class="projects__nav__index__txt">01</div>
                    <div class="projects__nav__index__line"></div>
                  </div>           
              </a>
            </li>
            <li class="projects__nav__item " data-id="salarybased">
              <a class="projects__nav__link" href="salarybased" data-ajax="false">
                <div class="projects__nav__index">
                  <div class="projects__nav__index__txt">02</div>
                  <div class="projects__nav__index__line"></div>
                </div>
              </a>
            </li>
        </ul>
        <hr/> 
      </nav>

      <section class="project" id="project-id-individual" data-title="Individual Loans" style="visibility: inherit">

       <div class="project__body">
          <h1 class="project__title">Money Link Individual Loans</h1>
          <div class="project__desc">
            <div class="project__desc__content">
              <ul>
                <li>Certified Copy of I.D</li>
                <li>Proof Of Residence</li>
                <li>Motor Vehicle</li>
                <li>Registration Book(If 3rd Party Plus Agreement Of Sale And Affidavit)</li>
                <li>Get 50% Hard Cash ($1000 Max) & 50% Bank Transfer</li>
              </ul>
              <br />
            </div> 
          </div>
        </div>

        <div class="project__media " data-first-media-index="0">
          <ul class="project__media__list">
              <li class="project__media__item project__media__item--image" style="">

                <img class=" project__media__img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" width="1778" height="1000" data-srcset="../../moneylenders/assets/img/ml.jpg 480w, ../../moneylenders/assets/img/ml.jpg 150w, ../../moneylenders/assets/img/ml.jpg 1280w, ../../moneylenders/assets/img/ml.jpg 1600w" data-sizes="auto" alt="" data-src="../../moneylenders/assets/img/ml.jpg">
                <noscript>
                  <img class="project__media__img" src="../../moneylenders/assets/img/ml.jpg" alt="" width="1778" height="1000">
                </noscript>    
              </li>
          </ul>
        </div>


        <ul class="project__tags">
          <div class="project__tags__bg"></div>
          <div class="project__tags__inner">
              <li class="project__tag">
                <div class="project__tag__name">Maximum Amount</div>
                <div class="project__tag__value">Determined By Vehicle Value</div>
              </li>
                    <li class="project__tag">
                <div class="project__tag__name">Loan Tenure</div>
                <div class="project__tag__value">6 Months Max</div>
              </li>
              <li class="project__tag">
                <div class="project__tag__name">Security</div>
                <div class="project__tag__value">Motor Vehicle</div>
              </li>
              <li class="project__tag">
                <div class="project__tag__name">Interest Rate</div>
                <div class="project__tag__value">5% / Month</div>
              </li>
          </div>
        </ul>
      </section>


      <section class="project" id="project-id-salarybased" data-title="Salary Based Loan" >

        <div class="project__body">
          <h1 class="project__title">SSB Salary Based Loans</h1>
          <div class="project__desc">
            <div class="project__desc__content">
              <ul>
                <li>Certified Copy Of I.D</li>
                <li>Current Pay Slip</li>
                <li>Proof Of Residence - Letter From Employer</li>
                <li>3 Months Bank Statement Or 3 Months Payslips</li>
                <li>Maximum Loan is 2.5 Times Net</li>
                <li>Salary.Minimum Loan is $50</li>
                <li>Get 50% Hard Cash ($500 Max) & 50% Bank transfer</li>
            </div>
          </div>
        </div>

        <div class="project__media " data-first-media-index="0">
          <ul class="project__media__list">
            <li class="project__media__item project__media__item--image" style="">
              <img class=" project__media__img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" width="1778" height="1000" data-srcset="../../moneylenders/assets/img/ml2.jpg 480w, ../../moneylenders/assets/img/ml2.jpg 150w, ../../moneylenders/assets/img/ml2.jpg 1280w, ../../moneylenders/assets/img/ml2.jpg 1600w" data-sizes="auto" alt="" data-src="../../moneylenders/assets/img/ml2.jpg">
              <noscript>
                <img class="project__media__img" src="../../moneylenders/assets/img/ml2.jpg" alt="" width="1600" height="1077">
              </noscript>            
            </li>
          </ul>
        </div>

        <ul class="project__tags">
          <div class="project__tags__bg"></div>
          <div class="project__tags__inner">
              <li class="project__tag">
                <div class="project__tag__name">Maximum Amount</div>
                <div class="project__tag__value">US$2 000</div>
              </li>
                    <li class="project__tag">
                <div class="project__tag__name">Loan Tenure</div>
                <div class="project__tag__value">12 Months Max</div>
              </li>
              <li class="project__tag">
                <div class="project__tag__name">Security</div>
                <div class="project__tag__value">Employer's Guarantee</div>
              </li>
              <li class="project__tag">
                <div class="project__tag__name">Interest Rate</div>
                <div class="project__tag__value">5% / month</div>
              </li>
          </div>
        </ul>
      </section>

      <div id="projects_media_overlay"></div>

      <div id="projects__scroll-cta">
        <div id="projects__scroll-cta__label" data-reverse="Back to the top">Scroll to explore</div>
        <button id="projects__scroll-cta__btn">
          <div id="projects__scroll-cta__picto">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="34" viewBox="0 0 20 34"><path fill-rule="evenodd" clip-rule="evenodd" d="M8 0v26.2l-8-8.1v5.7L10 34l10-10.2v-5.7l-8 8.1V0H8z"/></svg>    </div>
        </button>
      </div>
    </section>


    <section class="page" id="main-loading-page">
        <?php include '../inc/loading.php'; ?>
    </section>



   <!--  <section class="page" id="ajax-loading-page">
      <div id="ajax-loading__overlay"></div>
      <div id="ajax-loading__outer">
        <div id="ajax-loading__inner">
          <canvas id="ajax-loading__anim" width="288" height="127"></canvas>
          <div id="ajax-loading__txt">Loading</div>
        </div>
      </div>
    </section>     -->

</div>

    
        <script>
      var app = {
        debug: true,
        mode: "prod",
        baseUrl: "http://www.html.co.zw",
        rootUri: "",
        isMobile: false,
        isTablet: false
      };
    </script>

    <script src="../../code.jquery.com/jquery-3.1.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/lib/jquery-3.1.1.min.js"><\/script>')</script>

    <script src="../assets/js/scripts.js"></script>

  </body>


</html>
<!-- coded by alpham.co.zw -->