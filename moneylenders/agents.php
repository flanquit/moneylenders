<!-- coded by alpham.co.zw -->


<!-- This code currently has some errors on SAFARi browser so this php code below will bypass not allow safari browser to load the site by directing it to an error.php page  -->
<?php 
function get_browser_name($user_agent)
{
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';

    elseif (strpos($user_agent, 'Safari')) {
    header('location:error');  
    }

}

// Usage:

echo get_browser_name($_SERVER['HTTP_USER_AGENT']);


?>
<!DOCTYPE html>
<html class="no-js desktop ">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
   <?php include 'inc/head.php'; ?>  
    <!-- Latest compiled and minified CSS  | bootstrap CSS for the excel doc-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
</head>

  <body class="page-id-process">

          <a href="#" id="logo">
            <img src="assets/img/logo.png">
          </a>     

  <nav class="" id="nav">
    <?php include 'inc/nav2.php'; ?>
  </nav>

    <div id="page-container">
      
      <section class="page" id="process-page" data-title="">
        <header id="process__header">
        
          <h1 id="process__title">Our Agents<b class="dot2">.</b></h1>

          <div id="process__intro">
            <div class="embed-responsive embed-responsive-16by9">  
            <iframe class="embed-responsive-item" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vR_yz_sJy1aBZJ8TdZWY6Q4FpzUCb9pOultYpJ-cDxv7RHo2UBHdcYKe5LDEFCsOpGqfm711ReZMcB1/pubhtml?gid=1443409293&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
            </div>
          </div>
        </header>
      </section>


      <section class="page" id="main-loading-page">
          <?php include 'inc/loading.php'; ?>
      </section>

      

</div>
<footer id="footer">
  <?php include 'inc/footer.php'; ?>
</footer>

        <script>
      var app = {
        debug: true,
        mode: "prod",
        baseUrl: "http://www.html.co.zw",
        rootUri: "",
        isMobile: false,
        isTablet: false
      };
    </script>

                <script src="../code.jquery.com/jquery-3.1.1.min.js"></script>
            <script>window.jQuery || document.write('<script src="assets/js/lib/jquery-3.1.1.min.js"><\/script>')</script>

        <script src="assets/js/scripts.js"></script>

  </body>

</html>
<!-- coded by alpham.co.zw -->

