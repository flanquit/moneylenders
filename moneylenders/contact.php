<!-- coded by alpham.co.zw -->


<!-- This code currently has some errors on SAFARi browser so this php code below will bypass not allow safari browser to load the site by directing it to an error.php page  -->
<?php 
function get_browser_name($user_agent)
{
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';

    elseif (strpos($user_agent, 'Safari')) {
    header('location:error');  
    }

}

// Usage:

echo get_browser_name($_SERVER['HTTP_USER_AGENT']);


?>
<!DOCTYPE html>
<html class="no-js desktop ">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <?php include 'inc/head.php'; ?>   
</head>

<body class="page-id-contact">
  
    <a href="#" id="logo">
            <img src="assets/img/logo.png">
    </a>    

  <nav class="" id="nav">
    <?php include 'inc/nav2.php'; ?>
  </nav>

<div id="page-container"> 

  <section class="page" id="contact-page" data-title="Hammer and Tongues Contact Us">
    <header id="contact__header">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d949.5578348488436!2d31.053541829155122!3d-17.8277869992385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1931a4e7289fef57%3A0x2ba2ec7cbff9adda!2s100+Nelson+Mandela+Avenue%2C+Harare!5e0!3m2!1sen!2szw!4v1532435146213" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </header>  

  <form id="contact__form" action="#" method="#" >
    <div id="contact__form__alert"></div>

      <fieldset id="contact__form__info">
        <label class="contact__form__title contact__form__title--required" for="contact__form__info__email">Fill The Form Below</label>
        <div>
          <input id="contact__form__info__email" type="email" placeholder="Email" name="email" required>
          <input id="contact__form__info__phone" type="tel" placeholder="Phone" name="phone" required>
          <input id="contact__form__info__address" type="text" placeholder="Address" name="address">
          <input id="contact__form__info__phone" type="text" placeholder="Full Name" name="Full Name" required>
          
        </div>
      </fieldset>

 
  
      <fieldset id="contact__form__message">
        <label class="contact__form__title" for="contact__form__message__input">Your message</label>
        <textarea id="contact__form__message__input" placeholder="Type your request here" name="message"></textarea>
      </fieldset>
    
      <button class="contact__form__btn" id="contact__form__submit" type="submit" >
        <div>
          <span id="contact__form__submit__picto"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 18"><path d="M0 18l21-9L0 0v7l15 2-15 2v7z"/><path fill="none" d="M-2-3h24v24H-2V-3z"/></svg></span>
          <span id="contact__form__submit__label">Send message</span>
        </div>
      </button>

      <div id="contact__form__file">
        <input type="file" id="contact__form__file__input" name="file">
      </div>
  </form>
 
  
          
        </li>
            <li>
        <div>
        </div>
      </li>
    </ul>
  </div>
</div>
</section>


      <section class="page" id="main-loading-page">
          <?php include 'inc/loading.php'; ?>
      </section>
     


</div>

    <footer id="footer">
        <?php include 'inc/footer.php'; ?>
    </footer>

        <script>
      var app = {
        debug: true,
        mode: "prod",
        baseUrl: "http://www.html.co.zw",
        rootUri: "",
        isMobile: false,
        isTablet: false
      };
    </script>

                <script src="../code.jquery.com/jquery-3.1.1.min.js"></script>
            <script>window.jQuery || document.write('<script src="assets/js/lib/jquery-3.1.1.min.js"><\/script>')</script>

        <script src="assets/js/scripts.js"></script>

  </body>

</html>
<!-- coded by alpham.co.zw -->

